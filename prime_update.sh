#!/bin/bash

USAGE="(basename "$0") version(v2.x.x[.x]) [-h] [-i is initial deployment or not]-- script to update motion service

where:
    -h ./prime_update.sh v2.x.x[.x] 
    -i is initial deployment or not(default: false) "

INITIAL_DEPLOY=false

while getopts 'hi' option; do
  case "$option" in
    h) echo "$USAGE"
       exit
       ;;
    i) INITIAL_DEPLOY=true
       ;;
   \?) printf "illegal option: -%s\n" "$OPTARG" >&2
       echo "$USAGE" >&2
       exit 1
       ;;
  esac
done
shift $((OPTIND - 1))

set -eu

if [[ "$#" -lt 1 ]]; then
    echo "Illegal number of argument, you should provide the version number you want to update!"
    exit 1
fi

VERSION=$1
if [[ ! $VERSION =~ ^v[1-9].[0-9]+.[0-9]+(.[0-9]+)?$ ]]; then 
  echo "Input version number is not valid, valid version is v2.x.x[.x]"
  exit 1
fi

ROBOT_ROOT=$HOME/PrimeRobot
ROBOT_INSTALL=$ROBOT_ROOT/install
ROBOT_SERVICE=prime_robot.service
REPOSITORY="$ROBOT_ROOT/robot_system_update_repository"

# check robot service first
if sudo systemctl is-active --quiet "$ROBOT_SERVICE" ; then
  echo "Robot service: $ROBOT_SERVICE is running, please stop it first by running: systemctl stop $ROBOT_SERVICE"
  exit 1
fi

if [ ! -d "$ROBOT_ROOT" ]; then
  echo "$ROBOT_ROOT does not exist, will create it first"
  mkdir -p $ROBOT_ROOT 
fi 

if [ -d "$REPOSITORY" ]; then
  echo "$REPOSITORY exists, will remove it first."
  rm -rf $REPOSITORY
fi 

# Check Ubuntu version
UBUNTU_VERSION=$(lsb_release -rs)
echo "Ubuntu version $UBUNTU_VERSION detected." 
if [ $UBUNTU_VERSION = "18.04" ]; then
  TAG=$VERSION
else
  TAG=${VERSION}-${UBUNTU_VERSION}
fi

cd $ROBOT_ROOT 
git clone --branch $TAG https://jack_prime@bitbucket.org/primerobotics/robot_system_update_repository.git --depth 1
cd $REPOSITORY
# git checkout $VERSION 
tar -xzf prime_deploy.tar.gz

cd $ROBOT_ROOT
if [ -d "$ROBOT_INSTALL" ]; then
  echo "$ROBOT_INSTALL directory already exists, will remove it first!"
  rm -rf $ROBOT_INSTALL
fi 

mv $REPOSITORY/install $ROBOT_ROOT 
chmod +x -R install 

if [[ $INITIAL_DEPLOY == "true" ]]; then 
  echo "This is the initial deployment, will copy config and start_up directories." 
  mv $REPOSITORY/config $REPOSITORY/start_up $ROBOT_ROOT
  chmod +x -R start_up
fi 

echo "Motion service update to version: $TAG finished, please start the service by running: systemctl start $ROBOT_SERVICE"