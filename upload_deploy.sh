#!/bin/bash

USAGE="(basename "$0") version(v2.x.x[.x]) [-h]-- script to upload motion deployment files to remote repository

where:
    -h ./upload_deploy.sh v2.x.x[.x] "

REPOSITORY="$HOME/robot_system_update_repository"

while getopts ':h' option; do
  case "$option" in
    h) echo "$USAGE"
       exit
       ;;
    :) printf "missing argument for -%s\n" "$OPTARG" >&2
       echo "$USAGE" >&2
       exit 1
       ;;
   \?) printf "illegal option: -%s\n" "$OPTARG" >&2
       echo "$USAGE" >&2
       exit 1
       ;;
  esac
done
shift $((OPTIND - 1))

if [[ "$#" -lt 1 ]]; then
    echo "Illegal number of argument, you should provide the version number!"
    exit 1
fi

VERSION=$1
if [[ ! $VERSION =~ ^v[1-9].[0-9]+.[0-9]+(.[0-9]+)?$ ]]; then 
  echo "Input version number is not valid, valid version is v2.x.x[.x]"
  exit 1
fi

CURRENT_PATH=$(pwd)
echo "current path: $CURRENT_PATH"

if [ -d "$REPOSITORY" ]; then 
  echo "Remove existing repository: $REPOSITORY"
  rm -rf $REPOSITORY
fi

mkdir -p $REPOSITORY
git clone -b main git@bitbucket.org:primerobotics/robot_system_update_repository.git $REPOSITORY
cd $REPOSITORY

# Remove files before upload 
rm *.tar.gz *.yaml

cd $CURRENT_PATH
cd ../..
INSTALL=$(pwd)/install
CONFIG=$(pwd)/src/primebot/prime_launch/config 
START_UP=$(pwd)/src/primebot/start_up

if [ ! -d "$INSTALL" ]; then 
  echo "$INSTALL directory does not exist, please build and install first!"
  exit 1
fi 

cp -r $INSTALL $CONFIG $START_UP $REPOSITORY

cd $REPOSITORY
tar -czf prime_deploy.tar.gz install config start_up
rm -rf install config start_up

# Check Ubuntu version
UBUNTU_VERSION=$(lsb_release -rs)
echo "Ubuntu version $UBUNTU_VERSION detected." 
if [ $UBUNTU_VERSION = "18.04" ]; then
  TAG=$VERSION
else
  TAG=${VERSION}-${UBUNTU_VERSION}
fi

git add .
git commit -m "commit of version $TAG"
git tag -a $TAG -m "$TAG"
git push origin main --tags

echo "Upload $TAG to remote repository finished." 