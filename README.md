## This is the script to upgrade or downgrade robot system version

After v2.5.9， stop using them

Please go to the new address and use the new file to update motion 

new address: https://bitbucket.org/primerobotics/motion_update_script/src/main/

the old prime_update.sh can still be used for robot system 18.04

the new prime_update.sh can be used for all robot system(currently: 18.04, 20.04)